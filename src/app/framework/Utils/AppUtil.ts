export class AppUtil {
    static isNullOrEmpty(s: string) {
        if (s === undefined || s === null || s === '') {
            return true;
        } else {
            return false;
        }
    }

    static isNullEmpty(data: any){
        if(data === undefined || data === null){
            return true;
        }
        return false;
    }

    static isListNullOrEmpty(list: any[]){
        if(list === undefined || list === null || list.length === 0){
            return true;
        }
        return false;
    }
}