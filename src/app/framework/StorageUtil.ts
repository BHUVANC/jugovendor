import { classToPlain, plainToClass } from "class-transformer";
import { JsonParser } from "./ApiGenerator";


export class StorageUtil {



    static getAuthToken() {
        return this.getItem(KEYS.TOKEN);
    }

    static saveToken(token: string){
        this.setItem(KEYS.TOKEN, token);
    }

    static getUserId(){
        //TODO: remove hardcoded value = 1
        // return this.getItem(KEYS.USER_ID);
        return '1';
    }

    static logoutUser() {
        // logoutuser
        // localStorage.removeItem(KEYS.USER_DATA);
        this.clearAllData();
    }

    static clearAllData() {
        localStorage.clear()
    }



    static setItem(key: string, value: string) {
        localStorage.setItem(key, value)
    }
    static getItem(key: string) {
        return localStorage.getItem(key)
    }

}

export enum KEYS {
    TOKEN = "token",
    USER_ID = "userId",
}