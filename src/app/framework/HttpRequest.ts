import { Headers } from '@angular/http';
import { ClassType } from 'class-transformer/ClassTransformer';
import { BaseResponse } from './BaseResponseModel';
import { StorageUtil, KEYS } from './StorageUtil';

export class HttpRequest {

    url: string;
    params: any;
    method: string;
    taskCode: number;
    headers: Headers;
    classTypeValue: ClassType<any> = BaseResponse;
    isArrayResponse: false;

    constructor(url: string) {
        this.url = url;
        this.method = 'GET';
        this.headers = new Headers();
        this.addDefaultHeaders();
    }
    addDefaultHeaders() {
        this.headers.append('Content-Type', 'application/json');
        let token = StorageUtil.getAuthToken();
        console.log("Token Is: "+ token)
        if (token) {
            this.headers.append('Authorization', token);
        }
        let userId = StorageUtil.getUserId();
        console.log("UserId Is: "+ userId)
        if (userId) {
            this.headers.append(KEYS.USER_ID, userId);
        }
        this.headers.append('Access-Control-Allow-Origin', '*');

    }
    removeDefaultHeaders() {
        this.headers.delete('Content-Type');
        this.headers.delete('Authorization');
        this.headers.delete('roleType');
    }
    removeHeaders(key: string) {
        this.headers.delete(key);
    }
    addHeaders(key: string, value: string) {
        this.headers.append(key, value);
    }
    setPostMethod() {
        this.method = 'POST';
    }

    setDeleteMethod() {
        this.method = 'DELETE';
    }
    setPatchMethod() {
        this.method = 'PATCH';
    }
    setPutMethod() {
        this.method = 'PUT';
    }
}

export class HttpGenericRequest<T> extends HttpRequest {
    classType: ClassType<T>;
    constructor(url: string) {
        super(url);
    }
}
