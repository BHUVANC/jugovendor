import { TaskCode } from './globals';
import { AppComponent } from './../app.component';
import { JsonParser } from './ApiGenerator';
import { OnInit } from '@angular/core';
import { CommonService } from './common.service';

import { BaseResponse } from './BaseResponseModel';
import { HttpRequest, HttpGenericRequest } from './HttpRequest';
import { DownloadManager } from './DownloadManager';
import { StorageUtil } from './StorageUtil';


export class BaseComponent implements OnInit {
    constructor(protected commonService: CommonService) { }

    ngOnInit() {
    }

    downloadData(req: HttpRequest) {
        const manager = new DownloadManager(this, this.commonService);
        manager.downloadData(req);
    }

    onPreExecute(taskCode: TaskCode) {
        console.log('on preExecute of basecomponent');
        // showLoader()
    }

    onApiError(taskCode: TaskCode, error: any, req: HttpRequest) {
        if (error) {
            if (error._body) {
                const response = JsonParser.parseJsonString(JSON.parse(error._body), req.classTypeValue);
                console.log(response)
                if (response.code == '401') {
                    this.logoutUser(taskCode, response)
                }else{
                    this.onErrorReceived(taskCode, response);
                }
                
            } else {
                this.onServerError(taskCode, error, req);
            }
        } else {
            this.onServerError(taskCode, error, req);
        }
    }

    logoutUser(taskCode: TaskCode, response: any) {
        StorageUtil.clearAllData();
        // AppComponent.router.navigate(['../login']);
        if(response && response.message){
            alert(response.message)            
        }
    }

    onServerError(taskCode: TaskCode, error: any, req: HttpRequest) {
        alert("Oops some unknown error occurred");
    }

    onErrorReceived(taskCode: TaskCode, response: any) {
        console.log('on error recevied of base compo');
        // stopLoader()
    }

    logOut() { }

    onResponseReceived(taskCode: TaskCode, response: any) {
        console.log('onrespponse recevied of base compo');
        console.log(response);
        if (response instanceof BaseResponse) {
            if(response.error){
                return !response.error;
            }
        }
        return true;
        // stopLoader()
    }
}
