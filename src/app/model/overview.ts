export class overViewData{
    public tagline:string;
    public location:string;
    public contact:number;
    public website:string;
    public isEdit:boolean;
    public description:string;
    
  public get buttonText() {
 
      if(this.isEdit){
        return "Edit"
      }else{
        return "Save";
      }
    } 
    
  

    
}